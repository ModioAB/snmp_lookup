#
# PySNMP MIB module P-BRIDGE-MIB (http://snmplabs.com/pysmi)
# ASN.1 source file://data/mibs/P-BRIDGE-MIB
# Produced by pysmi-0.3.4 at Wed Jan 29 17:31:16 2020
# On host nerk platform Linux version 5.3.15-300.fc31.x86_64 by user spider
# Using Python version 3.7.5 (default, Oct 17 2019, 12:16:48) 
#
OctetString, Integer, ObjectIdentifier = mibBuilder.importSymbols("ASN1", "OctetString", "Integer", "ObjectIdentifier")
NamedValues, = mibBuilder.importSymbols("ASN1-ENUMERATION", "NamedValues")
ConstraintsUnion, ConstraintsIntersection, ValueSizeConstraint, ValueRangeConstraint, SingleValueConstraint = mibBuilder.importSymbols("ASN1-REFINEMENT", "ConstraintsUnion", "ConstraintsIntersection", "ValueSizeConstraint", "ValueRangeConstraint", "SingleValueConstraint")
dot1dBasePort, dot1dTp, dot1dBridge, dot1dTpPort, dot1dBasePortEntry = mibBuilder.importSymbols("BRIDGE-MIB", "dot1dBasePort", "dot1dTp", "dot1dBridge", "dot1dTpPort", "dot1dBasePortEntry")
ObjectGroup, ModuleCompliance, NotificationGroup = mibBuilder.importSymbols("SNMPv2-CONF", "ObjectGroup", "ModuleCompliance", "NotificationGroup")
ObjectIdentity, Counter32, NotificationType, Gauge32, TimeTicks, Unsigned32, Counter64, Integer32, Bits, MibIdentifier, MibScalar, MibTable, MibTableRow, MibTableColumn, IpAddress, ModuleIdentity, iso = mibBuilder.importSymbols("SNMPv2-SMI", "ObjectIdentity", "Counter32", "NotificationType", "Gauge32", "TimeTicks", "Unsigned32", "Counter64", "Integer32", "Bits", "MibIdentifier", "MibScalar", "MibTable", "MibTableRow", "MibTableColumn", "IpAddress", "ModuleIdentity", "iso")
TextualConvention, DisplayString, TimeInterval, MacAddress, TruthValue = mibBuilder.importSymbols("SNMPv2-TC", "TextualConvention", "DisplayString", "TimeInterval", "MacAddress", "TruthValue")
pBridgeMIB = ModuleIdentity((1, 3, 6, 1, 2, 1, 17, 6))
pBridgeMIB.setRevisions(('1999-08-25 00:00',))

if getattr(mibBuilder, 'version', (0, 0, 0)) > (4, 4, 0):
    if mibBuilder.loadTexts: pBridgeMIB.setRevisionsDescriptions(('Initial version, published as RFC 2674.',))
if mibBuilder.loadTexts: pBridgeMIB.setLastUpdated('9908250000Z')
if mibBuilder.loadTexts: pBridgeMIB.setOrganization('IETF Bridge MIB Working Group')
if mibBuilder.loadTexts: pBridgeMIB.setContactInfo(' Les Bell Postal: 3Com Europe Ltd. 3Com Centre, Boundary Way Hemel Hempstead, Herts. HP2 7YU UK Phone: +44 1442 438025 Email: Les_Bell@3Com.com Andrew Smith Postal: Extreme Networks 3585 Monroe St. Santa Clara CA 95051 USA Phone: +1 408 579 2821 Email: andrew@extremenetworks.com Paul Langille Postal: Newbridge Networks 5 Corporate Drive Andover, MA 01810 USA Phone: +1 978 691 4665 Email: langille@newbridge.com Anil Rijhsinghani Postal: Cabletron Systems 50 Minuteman Road Andover, MA 01810 USA Phone: +1 978 684 1295 Email: anil@cabletron.com Keith McCloghrie Postal: cisco Systems, Inc. 170 West Tasman Drive San Jose, CA 95134-1706 USA Phone: +1 408 526 5260 Email: kzm@cisco.com')
if mibBuilder.loadTexts: pBridgeMIB.setDescription('The Bridge MIB Extension module for managing Priority and Multicast Filtering, defined by IEEE 802.1D-1998.')
pBridgeMIBObjects = MibIdentifier((1, 3, 6, 1, 2, 1, 17, 6, 1))
class EnabledStatus(TextualConvention, Integer32):
    description = 'A simple status value for the object.'
    status = 'current'
    subtypeSpec = Integer32.subtypeSpec + ConstraintsUnion(SingleValueConstraint(1, 2))
    namedValues = NamedValues(("enabled", 1), ("disabled", 2))

dot1dExtBase = MibIdentifier((1, 3, 6, 1, 2, 1, 17, 6, 1, 1))
dot1dPriority = MibIdentifier((1, 3, 6, 1, 2, 1, 17, 6, 1, 2))
dot1dGarp = MibIdentifier((1, 3, 6, 1, 2, 1, 17, 6, 1, 3))
dot1dGmrp = MibIdentifier((1, 3, 6, 1, 2, 1, 17, 6, 1, 4))
dot1dDeviceCapabilities = MibScalar((1, 3, 6, 1, 2, 1, 17, 6, 1, 1, 1), Bits().clone(namedValues=NamedValues(("dot1dExtendedFilteringServices", 0), ("dot1dTrafficClasses", 1), ("dot1qStaticEntryIndividualPort", 2), ("dot1qIVLCapable", 3), ("dot1qSVLCapable", 4), ("dot1qHybridCapable", 5), ("dot1qConfigurablePvidTagging", 6), ("dot1dLocalVlanCapable", 7)))).setMaxAccess("readonly")
if mibBuilder.loadTexts: dot1dDeviceCapabilities.setReference('ISO/IEC 15802-3 Section 5.2, IEEE 802.1Q/D11 Section 5.2, 12.10.1.1.3/b/2')
if mibBuilder.loadTexts: dot1dDeviceCapabilities.setStatus('current')
if mibBuilder.loadTexts: dot1dDeviceCapabilities.setDescription('Indicates the optional parts of IEEE 802.1D and 802.1Q that are implemented by this device and are manageable through this MIB. Capabilities that are allowed on a per-port basis are indicated in dot1dPortCapabilities.')
dot1dTrafficClassesEnabled = MibScalar((1, 3, 6, 1, 2, 1, 17, 6, 1, 1, 2), TruthValue().clone('true')).setMaxAccess("readwrite")
if mibBuilder.loadTexts: dot1dTrafficClassesEnabled.setStatus('current')
if mibBuilder.loadTexts: dot1dTrafficClassesEnabled.setDescription('The value true(1) indicates that Traffic Classes are enabled on this bridge. When false(2), the bridge operates with a single priority level for all traffic.')
dot1dGmrpStatus = MibScalar((1, 3, 6, 1, 2, 1, 17, 6, 1, 1, 3), EnabledStatus().clone('enabled')).setMaxAccess("readwrite")
if mibBuilder.loadTexts: dot1dGmrpStatus.setStatus('current')
if mibBuilder.loadTexts: dot1dGmrpStatus.setDescription('The administrative status requested by management for GMRP. The value enabled(1) indicates that GMRP should be enabled on this device, in all VLANs, on all ports for which it has not been specifically disabled. When disabled(2), GMRP is disabled, in all VLANs, on all ports and all GMRP packets will be forwarded transparently. This object affects both Applicant and Registrar state machines. A transition from disabled(2) to enabled(1) will cause a reset of all GMRP state machines on all ports.')
dot1dPortCapabilitiesTable = MibTable((1, 3, 6, 1, 2, 1, 17, 6, 1, 1, 4), )
if mibBuilder.loadTexts: dot1dPortCapabilitiesTable.setStatus('current')
if mibBuilder.loadTexts: dot1dPortCapabilitiesTable.setDescription('A table that contains capabilities information about every port that is associated with this bridge.')
dot1dPortCapabilitiesEntry = MibTableRow((1, 3, 6, 1, 2, 1, 17, 6, 1, 1, 4, 1), )
dot1dBasePortEntry.registerAugmentions(("P-BRIDGE-MIB", "dot1dPortCapabilitiesEntry"))
dot1dPortCapabilitiesEntry.setIndexNames(*dot1dBasePortEntry.getIndexNames())
if mibBuilder.loadTexts: dot1dPortCapabilitiesEntry.setStatus('current')
if mibBuilder.loadTexts: dot1dPortCapabilitiesEntry.setDescription('A set of capabilities information about this port indexed by dot1dBasePort.')
dot1dPortCapabilities = MibTableColumn((1, 3, 6, 1, 2, 1, 17, 6, 1, 1, 4, 1, 1), Bits().clone(namedValues=NamedValues(("dot1qDot1qTagging", 0), ("dot1qConfigurableAcceptableFrameTypes", 1), ("dot1qIngressFiltering", 2)))).setMaxAccess("readonly")
if mibBuilder.loadTexts: dot1dPortCapabilities.setReference('ISO/IEC 15802-3 Section 5.2, IEEE 802.1Q/D11 Section 5.2')
if mibBuilder.loadTexts: dot1dPortCapabilities.setStatus('current')
if mibBuilder.loadTexts: dot1dPortCapabilities.setDescription('Indicates the parts of IEEE 802.1D and 802.1Q that are optional on a per-port basis that are implemented by this device and are manageable through this MIB.')
dot1dPortPriorityTable = MibTable((1, 3, 6, 1, 2, 1, 17, 6, 1, 2, 1), )
if mibBuilder.loadTexts: dot1dPortPriorityTable.setStatus('current')
if mibBuilder.loadTexts: dot1dPortPriorityTable.setDescription('A table that contains information about every port that is associated with this transparent bridge.')
dot1dPortPriorityEntry = MibTableRow((1, 3, 6, 1, 2, 1, 17, 6, 1, 2, 1, 1), )
dot1dBasePortEntry.registerAugmentions(("P-BRIDGE-MIB", "dot1dPortPriorityEntry"))
dot1dPortPriorityEntry.setIndexNames(*dot1dBasePortEntry.getIndexNames())
if mibBuilder.loadTexts: dot1dPortPriorityEntry.setStatus('current')
if mibBuilder.loadTexts: dot1dPortPriorityEntry.setDescription('A list of Default User Priorities for each port of a transparent bridge. This is indexed by dot1dBasePort.')
dot1dPortDefaultUserPriority = MibTableColumn((1, 3, 6, 1, 2, 1, 17, 6, 1, 2, 1, 1, 1), Integer32().subtype(subtypeSpec=ValueRangeConstraint(0, 7))).setMaxAccess("readwrite")
if mibBuilder.loadTexts: dot1dPortDefaultUserPriority.setStatus('current')
if mibBuilder.loadTexts: dot1dPortDefaultUserPriority.setDescription('The default ingress User Priority for this port. This only has effect on media, such as Ethernet, that do not support native User Priority.')
dot1dPortNumTrafficClasses = MibTableColumn((1, 3, 6, 1, 2, 1, 17, 6, 1, 2, 1, 1, 2), Integer32().subtype(subtypeSpec=ValueRangeConstraint(1, 8))).setMaxAccess("readwrite")
if mibBuilder.loadTexts: dot1dPortNumTrafficClasses.setStatus('current')
if mibBuilder.loadTexts: dot1dPortNumTrafficClasses.setDescription('The number of egress traffic classes supported on this port. This object may optionally be read-only.')
dot1dUserPriorityRegenTable = MibTable((1, 3, 6, 1, 2, 1, 17, 6, 1, 2, 2), )
if mibBuilder.loadTexts: dot1dUserPriorityRegenTable.setReference('ISO/IEC 15802-3 Section 6.4')
if mibBuilder.loadTexts: dot1dUserPriorityRegenTable.setStatus('current')
if mibBuilder.loadTexts: dot1dUserPriorityRegenTable.setDescription('A list of Regenerated User Priorities for each received User Priority on each port of a bridge. The Regenerated User Priority value may be used to index the Traffic Class Table for each input port. This only has effect on media that support native User Priority. The default values for Regenerated User Priorities are the same as the User Priorities.')
dot1dUserPriorityRegenEntry = MibTableRow((1, 3, 6, 1, 2, 1, 17, 6, 1, 2, 2, 1), ).setIndexNames((0, "BRIDGE-MIB", "dot1dBasePort"), (0, "P-BRIDGE-MIB", "dot1dUserPriority"))
if mibBuilder.loadTexts: dot1dUserPriorityRegenEntry.setStatus('current')
if mibBuilder.loadTexts: dot1dUserPriorityRegenEntry.setDescription('A mapping of incoming User Priority to a Regenerated User Priority.')
dot1dUserPriority = MibTableColumn((1, 3, 6, 1, 2, 1, 17, 6, 1, 2, 2, 1, 1), Integer32().subtype(subtypeSpec=ValueRangeConstraint(0, 7)))
if mibBuilder.loadTexts: dot1dUserPriority.setStatus('current')
if mibBuilder.loadTexts: dot1dUserPriority.setDescription('The User Priority for a frame received on this port.')
dot1dRegenUserPriority = MibTableColumn((1, 3, 6, 1, 2, 1, 17, 6, 1, 2, 2, 1, 2), Integer32().subtype(subtypeSpec=ValueRangeConstraint(0, 7))).setMaxAccess("readwrite")
if mibBuilder.loadTexts: dot1dRegenUserPriority.setStatus('current')
if mibBuilder.loadTexts: dot1dRegenUserPriority.setDescription('The Regenerated User Priority the incoming User Priority is mapped to for this port.')
dot1dTrafficClassTable = MibTable((1, 3, 6, 1, 2, 1, 17, 6, 1, 2, 3), )
if mibBuilder.loadTexts: dot1dTrafficClassTable.setReference('ISO/IEC 15802-3 Table 7-2')
if mibBuilder.loadTexts: dot1dTrafficClassTable.setStatus('current')
if mibBuilder.loadTexts: dot1dTrafficClassTable.setDescription('A table mapping evaluated User Priority to Traffic Class, for forwarding by the bridge. Traffic class is a number in the range (0..(dot1dPortNumTrafficClasses-1)).')
dot1dTrafficClassEntry = MibTableRow((1, 3, 6, 1, 2, 1, 17, 6, 1, 2, 3, 1), ).setIndexNames((0, "BRIDGE-MIB", "dot1dBasePort"), (0, "P-BRIDGE-MIB", "dot1dTrafficClassPriority"))
if mibBuilder.loadTexts: dot1dTrafficClassEntry.setStatus('current')
if mibBuilder.loadTexts: dot1dTrafficClassEntry.setDescription('User Priority to Traffic Class mapping.')
dot1dTrafficClassPriority = MibTableColumn((1, 3, 6, 1, 2, 1, 17, 6, 1, 2, 3, 1, 1), Integer32().subtype(subtypeSpec=ValueRangeConstraint(0, 7)))
if mibBuilder.loadTexts: dot1dTrafficClassPriority.setStatus('current')
if mibBuilder.loadTexts: dot1dTrafficClassPriority.setDescription('The Priority value determined for the received frame. This value is equivalent to the priority indicated in the tagged frame received, or one of the evaluated priorities, determined according to the media-type. For untagged frames received from Ethernet media, this value is equal to the dot1dPortDefaultUserPriority value for the ingress port. For untagged frames received from non-Ethernet media, this value is equal to the dot1dRegenUserPriority value for the ingress port and media-specific user priority.')
dot1dTrafficClass = MibTableColumn((1, 3, 6, 1, 2, 1, 17, 6, 1, 2, 3, 1, 2), Integer32().subtype(subtypeSpec=ValueRangeConstraint(0, 7))).setMaxAccess("readwrite")
if mibBuilder.loadTexts: dot1dTrafficClass.setStatus('current')
if mibBuilder.loadTexts: dot1dTrafficClass.setDescription('The Traffic Class the received frame is mapped to.')
dot1dPortOutboundAccessPriorityTable = MibTable((1, 3, 6, 1, 2, 1, 17, 6, 1, 2, 4), )
if mibBuilder.loadTexts: dot1dPortOutboundAccessPriorityTable.setReference('ISO/IEC 15802-3 Table 7-3')
if mibBuilder.loadTexts: dot1dPortOutboundAccessPriorityTable.setStatus('current')
if mibBuilder.loadTexts: dot1dPortOutboundAccessPriorityTable.setDescription('A table mapping Regenerated User Priority to Outbound Access Priority. This is a fixed mapping for all port types, with two options for 802.5 Token Ring.')
dot1dPortOutboundAccessPriorityEntry = MibTableRow((1, 3, 6, 1, 2, 1, 17, 6, 1, 2, 4, 1), ).setIndexNames((0, "BRIDGE-MIB", "dot1dBasePort"), (0, "P-BRIDGE-MIB", "dot1dRegenUserPriority"))
if mibBuilder.loadTexts: dot1dPortOutboundAccessPriorityEntry.setStatus('current')
if mibBuilder.loadTexts: dot1dPortOutboundAccessPriorityEntry.setDescription('Regenerated User Priority to Outbound Access Priority mapping.')
dot1dPortOutboundAccessPriority = MibTableColumn((1, 3, 6, 1, 2, 1, 17, 6, 1, 2, 4, 1, 1), Integer32().subtype(subtypeSpec=ValueRangeConstraint(0, 7))).setMaxAccess("readonly")
if mibBuilder.loadTexts: dot1dPortOutboundAccessPriority.setStatus('current')
if mibBuilder.loadTexts: dot1dPortOutboundAccessPriority.setDescription('The Outbound Access Priority the received frame is mapped to.')
dot1dPortGarpTable = MibTable((1, 3, 6, 1, 2, 1, 17, 6, 1, 3, 1), )
if mibBuilder.loadTexts: dot1dPortGarpTable.setStatus('current')
if mibBuilder.loadTexts: dot1dPortGarpTable.setDescription('A table of GARP control information about every bridge port. This is indexed by dot1dBasePort.')
dot1dPortGarpEntry = MibTableRow((1, 3, 6, 1, 2, 1, 17, 6, 1, 3, 1, 1), )
dot1dBasePortEntry.registerAugmentions(("P-BRIDGE-MIB", "dot1dPortGarpEntry"))
dot1dPortGarpEntry.setIndexNames(*dot1dBasePortEntry.getIndexNames())
if mibBuilder.loadTexts: dot1dPortGarpEntry.setStatus('current')
if mibBuilder.loadTexts: dot1dPortGarpEntry.setDescription('GARP control information for a bridge port.')
dot1dPortGarpJoinTime = MibTableColumn((1, 3, 6, 1, 2, 1, 17, 6, 1, 3, 1, 1, 1), TimeInterval().clone(20)).setMaxAccess("readwrite")
if mibBuilder.loadTexts: dot1dPortGarpJoinTime.setStatus('current')
if mibBuilder.loadTexts: dot1dPortGarpJoinTime.setDescription('The GARP Join time, in centiseconds. The GARP Leave Timer must be greater than or equal to three times the GARP Join Timer.')
dot1dPortGarpLeaveTime = MibTableColumn((1, 3, 6, 1, 2, 1, 17, 6, 1, 3, 1, 1, 2), TimeInterval().clone(60)).setMaxAccess("readwrite")
if mibBuilder.loadTexts: dot1dPortGarpLeaveTime.setStatus('current')
if mibBuilder.loadTexts: dot1dPortGarpLeaveTime.setDescription('The GARP Leave time, in centiseconds. The GARP LeaveAll Timer must be greater than the GARP Leave Timer. Also, the GARP Leave Timer must be greater than or equal to three times the GARP Join Timer.')
dot1dPortGarpLeaveAllTime = MibTableColumn((1, 3, 6, 1, 2, 1, 17, 6, 1, 3, 1, 1, 3), TimeInterval().clone(1000)).setMaxAccess("readwrite")
if mibBuilder.loadTexts: dot1dPortGarpLeaveAllTime.setStatus('current')
if mibBuilder.loadTexts: dot1dPortGarpLeaveAllTime.setDescription('The GARP LeaveAll time, in centiseconds. The GARP LeaveAll Timer must be greater than the GARP Leave Timer.')
dot1dPortGmrpTable = MibTable((1, 3, 6, 1, 2, 1, 17, 6, 1, 4, 1), )
if mibBuilder.loadTexts: dot1dPortGmrpTable.setStatus('current')
if mibBuilder.loadTexts: dot1dPortGmrpTable.setDescription('A table of GMRP control and status information about every bridge port. Augments the dot1dBasePortTable.')
dot1dPortGmrpEntry = MibTableRow((1, 3, 6, 1, 2, 1, 17, 6, 1, 4, 1, 1), )
dot1dBasePortEntry.registerAugmentions(("P-BRIDGE-MIB", "dot1dPortGmrpEntry"))
dot1dPortGmrpEntry.setIndexNames(*dot1dBasePortEntry.getIndexNames())
if mibBuilder.loadTexts: dot1dPortGmrpEntry.setStatus('current')
if mibBuilder.loadTexts: dot1dPortGmrpEntry.setDescription('GMRP control and status information for a bridge port.')
dot1dPortGmrpStatus = MibTableColumn((1, 3, 6, 1, 2, 1, 17, 6, 1, 4, 1, 1, 1), EnabledStatus().clone('enabled')).setMaxAccess("readwrite")
if mibBuilder.loadTexts: dot1dPortGmrpStatus.setStatus('current')
if mibBuilder.loadTexts: dot1dPortGmrpStatus.setDescription('The administrative state of GMRP operation on this port. The value enabled(1) indicates that GMRP is enabled on this port in all VLANs as long as dot1dGmrpStatus is also enabled(1). A value of disabled(2) indicates that GMRP is disabled on this port in all VLANs: any GMRP packets received will be silently discarded and no GMRP registrations will be propagated from other ports. Setting this to a value of enabled(1) will be stored by the agent but will only take effect on the GMRP protocol operation if dot1dGmrpStatus also indicates the value enabled(1). This object affects all GMRP Applicant and Registrar state machines on this port. A transition from disabled(2) to enabled(1) will cause a reset of all GMRP state machines on this port.')
dot1dPortGmrpFailedRegistrations = MibTableColumn((1, 3, 6, 1, 2, 1, 17, 6, 1, 4, 1, 1, 2), Counter32()).setMaxAccess("readonly")
if mibBuilder.loadTexts: dot1dPortGmrpFailedRegistrations.setStatus('current')
if mibBuilder.loadTexts: dot1dPortGmrpFailedRegistrations.setDescription('The total number of failed GMRP registrations, for any reason, in all VLANs, on this port.')
dot1dPortGmrpLastPduOrigin = MibTableColumn((1, 3, 6, 1, 2, 1, 17, 6, 1, 4, 1, 1, 3), MacAddress()).setMaxAccess("readonly")
if mibBuilder.loadTexts: dot1dPortGmrpLastPduOrigin.setStatus('current')
if mibBuilder.loadTexts: dot1dPortGmrpLastPduOrigin.setDescription('The Source MAC Address of the last GMRP message received on this port.')
dot1dTpHCPortTable = MibTable((1, 3, 6, 1, 2, 1, 17, 4, 5), )
if mibBuilder.loadTexts: dot1dTpHCPortTable.setStatus('current')
if mibBuilder.loadTexts: dot1dTpHCPortTable.setDescription('A table that contains information about every high capacity port that is associated with this transparent bridge.')
dot1dTpHCPortEntry = MibTableRow((1, 3, 6, 1, 2, 1, 17, 4, 5, 1), ).setIndexNames((0, "BRIDGE-MIB", "dot1dTpPort"))
if mibBuilder.loadTexts: dot1dTpHCPortEntry.setStatus('current')
if mibBuilder.loadTexts: dot1dTpHCPortEntry.setDescription('Statistics information for each high capacity port of a transparent bridge.')
dot1dTpHCPortInFrames = MibTableColumn((1, 3, 6, 1, 2, 1, 17, 4, 5, 1, 1), Counter64()).setMaxAccess("readonly")
if mibBuilder.loadTexts: dot1dTpHCPortInFrames.setReference('ISO/IEC 15802-3 Section 14.6.1.1.3')
if mibBuilder.loadTexts: dot1dTpHCPortInFrames.setStatus('current')
if mibBuilder.loadTexts: dot1dTpHCPortInFrames.setDescription('The number of frames that have been received by this port from its segment. Note that a frame received on the interface corresponding to this port is only counted by this object if and only if it is for a protocol being processed by the local bridging function, including bridge management frames.')
dot1dTpHCPortOutFrames = MibTableColumn((1, 3, 6, 1, 2, 1, 17, 4, 5, 1, 2), Counter64()).setMaxAccess("readonly")
if mibBuilder.loadTexts: dot1dTpHCPortOutFrames.setReference('ISO/IEC 15802-3 Section 14.6.1.1.3')
if mibBuilder.loadTexts: dot1dTpHCPortOutFrames.setStatus('current')
if mibBuilder.loadTexts: dot1dTpHCPortOutFrames.setDescription('The number of frames that have been transmitted by this port to its segment. Note that a frame transmitted on the interface corresponding to this port is only counted by this object if and only if it is for a protocol being processed by the local bridging function, including bridge management frames.')
dot1dTpHCPortInDiscards = MibTableColumn((1, 3, 6, 1, 2, 1, 17, 4, 5, 1, 3), Counter64()).setMaxAccess("readonly")
if mibBuilder.loadTexts: dot1dTpHCPortInDiscards.setReference('ISO/IEC 15802-3 Section 14.6.1.1.3')
if mibBuilder.loadTexts: dot1dTpHCPortInDiscards.setStatus('current')
if mibBuilder.loadTexts: dot1dTpHCPortInDiscards.setDescription('Count of valid frames that have been received by this port from its segment which were discarded (i.e., filtered) by the Forwarding Process.')
dot1dTpPortOverflowTable = MibTable((1, 3, 6, 1, 2, 1, 17, 4, 6), )
if mibBuilder.loadTexts: dot1dTpPortOverflowTable.setStatus('current')
if mibBuilder.loadTexts: dot1dTpPortOverflowTable.setDescription('A table that contains the most-significant bits of statistics counters for ports that are associated with this transparent bridge that are on high capacity interfaces, as defined in the conformance clauses for this table. This table is provided as a way to read 64-bit counters for agents which support only SNMPv1. Note that the reporting of most-significant and least-significant counter bits separately runs the risk of missing an overflow of the lower bits in the interval between sampling. The manager must be aware of this possibility, even within the same varbindlist, when interpreting the results of a request or asynchronous notification.')
dot1dTpPortOverflowEntry = MibTableRow((1, 3, 6, 1, 2, 1, 17, 4, 6, 1), ).setIndexNames((0, "BRIDGE-MIB", "dot1dTpPort"))
if mibBuilder.loadTexts: dot1dTpPortOverflowEntry.setStatus('current')
if mibBuilder.loadTexts: dot1dTpPortOverflowEntry.setDescription('The most significant bits of statistics counters for a high capacity interface of a transparent bridge. Each object is associated with a corresponding object in dot1dTpPortTable which indicates the least significant bits of the counter.')
dot1dTpPortInOverflowFrames = MibTableColumn((1, 3, 6, 1, 2, 1, 17, 4, 6, 1, 1), Counter32()).setMaxAccess("readonly")
if mibBuilder.loadTexts: dot1dTpPortInOverflowFrames.setReference('ISO/IEC 15802-3 Section 14.6.1.1.3')
if mibBuilder.loadTexts: dot1dTpPortInOverflowFrames.setStatus('current')
if mibBuilder.loadTexts: dot1dTpPortInOverflowFrames.setDescription('The number of times the associated dot1dTpPortInFrames counter has overflowed.')
dot1dTpPortOutOverflowFrames = MibTableColumn((1, 3, 6, 1, 2, 1, 17, 4, 6, 1, 2), Counter32()).setMaxAccess("readonly")
if mibBuilder.loadTexts: dot1dTpPortOutOverflowFrames.setReference('ISO/IEC 15802-3 Section 14.6.1.1.3')
if mibBuilder.loadTexts: dot1dTpPortOutOverflowFrames.setStatus('current')
if mibBuilder.loadTexts: dot1dTpPortOutOverflowFrames.setDescription('The number of times the associated dot1dTpPortOutFrames counter has overflowed.')
dot1dTpPortInOverflowDiscards = MibTableColumn((1, 3, 6, 1, 2, 1, 17, 4, 6, 1, 3), Counter32()).setMaxAccess("readonly")
if mibBuilder.loadTexts: dot1dTpPortInOverflowDiscards.setReference('ISO/IEC 15802-3 Section 14.6.1.1.3')
if mibBuilder.loadTexts: dot1dTpPortInOverflowDiscards.setStatus('current')
if mibBuilder.loadTexts: dot1dTpPortInOverflowDiscards.setDescription('The number of times the associated dot1dTpPortInDiscards counter has overflowed.')
pBridgeConformance = MibIdentifier((1, 3, 6, 1, 2, 1, 17, 6, 2))
pBridgeGroups = MibIdentifier((1, 3, 6, 1, 2, 1, 17, 6, 2, 1))
pBridgeCompliances = MibIdentifier((1, 3, 6, 1, 2, 1, 17, 6, 2, 2))
pBridgeExtCapGroup = ObjectGroup((1, 3, 6, 1, 2, 1, 17, 6, 2, 1, 1)).setObjects(("P-BRIDGE-MIB", "dot1dDeviceCapabilities"), ("P-BRIDGE-MIB", "dot1dPortCapabilities"))
if getattr(mibBuilder, 'version', (0, 0, 0)) > (4, 4, 0):
    pBridgeExtCapGroup = pBridgeExtCapGroup.setStatus('current')
if mibBuilder.loadTexts: pBridgeExtCapGroup.setDescription('A collection of objects indicating the optional capabilites of the device.')
pBridgeDeviceGmrpGroup = ObjectGroup((1, 3, 6, 1, 2, 1, 17, 6, 2, 1, 2)).setObjects(("P-BRIDGE-MIB", "dot1dGmrpStatus"))
if getattr(mibBuilder, 'version', (0, 0, 0)) > (4, 4, 0):
    pBridgeDeviceGmrpGroup = pBridgeDeviceGmrpGroup.setStatus('current')
if mibBuilder.loadTexts: pBridgeDeviceGmrpGroup.setDescription('A collection of objects providing device-level control for the Multicast Filtering extended bridge services.')
pBridgeDevicePriorityGroup = ObjectGroup((1, 3, 6, 1, 2, 1, 17, 6, 2, 1, 3)).setObjects(("P-BRIDGE-MIB", "dot1dTrafficClassesEnabled"))
if getattr(mibBuilder, 'version', (0, 0, 0)) > (4, 4, 0):
    pBridgeDevicePriorityGroup = pBridgeDevicePriorityGroup.setStatus('current')
if mibBuilder.loadTexts: pBridgeDevicePriorityGroup.setDescription('A collection of objects providing device-level control for the Priority services.')
pBridgeDefaultPriorityGroup = ObjectGroup((1, 3, 6, 1, 2, 1, 17, 6, 2, 1, 4)).setObjects(("P-BRIDGE-MIB", "dot1dPortDefaultUserPriority"))
if getattr(mibBuilder, 'version', (0, 0, 0)) > (4, 4, 0):
    pBridgeDefaultPriorityGroup = pBridgeDefaultPriorityGroup.setStatus('current')
if mibBuilder.loadTexts: pBridgeDefaultPriorityGroup.setDescription('A collection of objects defining the User Priority applicable to each port for media which do not support native User Priority.')
pBridgeRegenPriorityGroup = ObjectGroup((1, 3, 6, 1, 2, 1, 17, 6, 2, 1, 5)).setObjects(("P-BRIDGE-MIB", "dot1dRegenUserPriority"))
if getattr(mibBuilder, 'version', (0, 0, 0)) > (4, 4, 0):
    pBridgeRegenPriorityGroup = pBridgeRegenPriorityGroup.setStatus('current')
if mibBuilder.loadTexts: pBridgeRegenPriorityGroup.setDescription('A collection of objects defining the User Priorities applicable to each port for media which support native User Priority.')
pBridgePriorityGroup = ObjectGroup((1, 3, 6, 1, 2, 1, 17, 6, 2, 1, 6)).setObjects(("P-BRIDGE-MIB", "dot1dPortNumTrafficClasses"), ("P-BRIDGE-MIB", "dot1dTrafficClass"))
if getattr(mibBuilder, 'version', (0, 0, 0)) > (4, 4, 0):
    pBridgePriorityGroup = pBridgePriorityGroup.setStatus('current')
if mibBuilder.loadTexts: pBridgePriorityGroup.setDescription('A collection of objects defining the traffic classes within a bridge for each evaluated User Priority.')
pBridgeAccessPriorityGroup = ObjectGroup((1, 3, 6, 1, 2, 1, 17, 6, 2, 1, 7)).setObjects(("P-BRIDGE-MIB", "dot1dPortOutboundAccessPriority"))
if getattr(mibBuilder, 'version', (0, 0, 0)) > (4, 4, 0):
    pBridgeAccessPriorityGroup = pBridgeAccessPriorityGroup.setStatus('current')
if mibBuilder.loadTexts: pBridgeAccessPriorityGroup.setDescription('A collection of objects defining the media dependent outbound access level for each priority.')
pBridgePortGarpGroup = ObjectGroup((1, 3, 6, 1, 2, 1, 17, 6, 2, 1, 8)).setObjects(("P-BRIDGE-MIB", "dot1dPortGarpJoinTime"), ("P-BRIDGE-MIB", "dot1dPortGarpLeaveTime"), ("P-BRIDGE-MIB", "dot1dPortGarpLeaveAllTime"))
if getattr(mibBuilder, 'version', (0, 0, 0)) > (4, 4, 0):
    pBridgePortGarpGroup = pBridgePortGarpGroup.setStatus('current')
if mibBuilder.loadTexts: pBridgePortGarpGroup.setDescription('A collection of objects providing port level control and status information for GARP operation.')
pBridgePortGmrpGroup = ObjectGroup((1, 3, 6, 1, 2, 1, 17, 6, 2, 1, 9)).setObjects(("P-BRIDGE-MIB", "dot1dPortGmrpStatus"), ("P-BRIDGE-MIB", "dot1dPortGmrpFailedRegistrations"), ("P-BRIDGE-MIB", "dot1dPortGmrpLastPduOrigin"))
if getattr(mibBuilder, 'version', (0, 0, 0)) > (4, 4, 0):
    pBridgePortGmrpGroup = pBridgePortGmrpGroup.setStatus('current')
if mibBuilder.loadTexts: pBridgePortGmrpGroup.setDescription('A collection of objects providing port level control and status information for GMRP operation.')
pBridgeHCPortGroup = ObjectGroup((1, 3, 6, 1, 2, 1, 17, 6, 2, 1, 10)).setObjects(("P-BRIDGE-MIB", "dot1dTpHCPortInFrames"), ("P-BRIDGE-MIB", "dot1dTpHCPortOutFrames"), ("P-BRIDGE-MIB", "dot1dTpHCPortInDiscards"))
if getattr(mibBuilder, 'version', (0, 0, 0)) > (4, 4, 0):
    pBridgeHCPortGroup = pBridgeHCPortGroup.setStatus('current')
if mibBuilder.loadTexts: pBridgeHCPortGroup.setDescription('A collection of objects providing 64-bit statistics counters for high capacity bridge ports.')
pBridgePortOverflowGroup = ObjectGroup((1, 3, 6, 1, 2, 1, 17, 6, 2, 1, 11)).setObjects(("P-BRIDGE-MIB", "dot1dTpPortInOverflowFrames"), ("P-BRIDGE-MIB", "dot1dTpPortOutOverflowFrames"), ("P-BRIDGE-MIB", "dot1dTpPortInOverflowDiscards"))
if getattr(mibBuilder, 'version', (0, 0, 0)) > (4, 4, 0):
    pBridgePortOverflowGroup = pBridgePortOverflowGroup.setStatus('current')
if mibBuilder.loadTexts: pBridgePortOverflowGroup.setDescription('A collection of objects providing overflow statistics counters for high capacity bridge ports.')
pBridgeCompliance = ModuleCompliance((1, 3, 6, 1, 2, 1, 17, 6, 2, 2, 1)).setObjects(("P-BRIDGE-MIB", "pBridgeExtCapGroup"), ("P-BRIDGE-MIB", "pBridgeDeviceGmrpGroup"), ("P-BRIDGE-MIB", "pBridgeDevicePriorityGroup"), ("P-BRIDGE-MIB", "pBridgeDefaultPriorityGroup"), ("P-BRIDGE-MIB", "pBridgeRegenPriorityGroup"), ("P-BRIDGE-MIB", "pBridgePriorityGroup"), ("P-BRIDGE-MIB", "pBridgeAccessPriorityGroup"), ("P-BRIDGE-MIB", "pBridgePortGarpGroup"), ("P-BRIDGE-MIB", "pBridgePortGmrpGroup"), ("P-BRIDGE-MIB", "pBridgeHCPortGroup"), ("P-BRIDGE-MIB", "pBridgePortOverflowGroup"))

if getattr(mibBuilder, 'version', (0, 0, 0)) > (4, 4, 0):
    pBridgeCompliance = pBridgeCompliance.setStatus('current')
if mibBuilder.loadTexts: pBridgeCompliance.setDescription('The compliance statement for device support of Priority and Multicast Filtering extended bridging services.')
mibBuilder.exportSymbols("P-BRIDGE-MIB", dot1dDeviceCapabilities=dot1dDeviceCapabilities, dot1dPortGarpJoinTime=dot1dPortGarpJoinTime, dot1dPortGmrpLastPduOrigin=dot1dPortGmrpLastPduOrigin, pBridgeGroups=pBridgeGroups, pBridgeExtCapGroup=pBridgeExtCapGroup, dot1dTpHCPortInFrames=dot1dTpHCPortInFrames, dot1dPortNumTrafficClasses=dot1dPortNumTrafficClasses, dot1dPortGarpLeaveTime=dot1dPortGarpLeaveTime, dot1dTpHCPortOutFrames=dot1dTpHCPortOutFrames, dot1dGarp=dot1dGarp, dot1dPortOutboundAccessPriorityTable=dot1dPortOutboundAccessPriorityTable, dot1dPortOutboundAccessPriorityEntry=dot1dPortOutboundAccessPriorityEntry, dot1dPortGmrpTable=dot1dPortGmrpTable, pBridgePriorityGroup=pBridgePriorityGroup, pBridgeMIB=pBridgeMIB, pBridgePortGarpGroup=pBridgePortGarpGroup, dot1dGmrpStatus=dot1dGmrpStatus, dot1dUserPriorityRegenTable=dot1dUserPriorityRegenTable, dot1dTpPortOverflowEntry=dot1dTpPortOverflowEntry, dot1dTrafficClassesEnabled=dot1dTrafficClassesEnabled, dot1dTpPortInOverflowDiscards=dot1dTpPortInOverflowDiscards, dot1dPortCapabilitiesEntry=dot1dPortCapabilitiesEntry, EnabledStatus=EnabledStatus, pBridgeMIBObjects=pBridgeMIBObjects, dot1dPortGmrpStatus=dot1dPortGmrpStatus, pBridgeAccessPriorityGroup=pBridgeAccessPriorityGroup, pBridgeCompliance=pBridgeCompliance, pBridgeConformance=pBridgeConformance, dot1dUserPriority=dot1dUserPriority, dot1dPortGarpTable=dot1dPortGarpTable, dot1dPortCapabilitiesTable=dot1dPortCapabilitiesTable, dot1dPortOutboundAccessPriority=dot1dPortOutboundAccessPriority, dot1dTpHCPortTable=dot1dTpHCPortTable, pBridgeRegenPriorityGroup=pBridgeRegenPriorityGroup, dot1dTrafficClassPriority=dot1dTrafficClassPriority, dot1dExtBase=dot1dExtBase, dot1dTrafficClassEntry=dot1dTrafficClassEntry, dot1dPortGarpEntry=dot1dPortGarpEntry, dot1dPortCapabilities=dot1dPortCapabilities, pBridgeDevicePriorityGroup=pBridgeDevicePriorityGroup, pBridgeHCPortGroup=pBridgeHCPortGroup, dot1dPriority=dot1dPriority, dot1dTpHCPortInDiscards=dot1dTpHCPortInDiscards, dot1dTpPortOutOverflowFrames=dot1dTpPortOutOverflowFrames, dot1dPortPriorityTable=dot1dPortPriorityTable, dot1dTrafficClassTable=dot1dTrafficClassTable, pBridgeDefaultPriorityGroup=pBridgeDefaultPriorityGroup, dot1dPortGmrpFailedRegistrations=dot1dPortGmrpFailedRegistrations, pBridgeCompliances=pBridgeCompliances, dot1dUserPriorityRegenEntry=dot1dUserPriorityRegenEntry, dot1dPortGarpLeaveAllTime=dot1dPortGarpLeaveAllTime, dot1dTpPortOverflowTable=dot1dTpPortOverflowTable, dot1dGmrp=dot1dGmrp, pBridgePortOverflowGroup=pBridgePortOverflowGroup, PYSNMP_MODULE_ID=pBridgeMIB, dot1dPortDefaultUserPriority=dot1dPortDefaultUserPriority, pBridgeDeviceGmrpGroup=pBridgeDeviceGmrpGroup, dot1dTrafficClass=dot1dTrafficClass, dot1dRegenUserPriority=dot1dRegenUserPriority, pBridgePortGmrpGroup=pBridgePortGmrpGroup, dot1dTpPortInOverflowFrames=dot1dTpPortInOverflowFrames, dot1dPortPriorityEntry=dot1dPortPriorityEntry, dot1dPortGmrpEntry=dot1dPortGmrpEntry, dot1dTpHCPortEntry=dot1dTpHCPortEntry)
