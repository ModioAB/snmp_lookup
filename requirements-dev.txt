# Include requirements.txt
-r requirements.txt
# List all dependencies needed for test / development / lint / check here
twine 
wheel
setuptools >= 42
setuptools_scm
flake8 >= 3.8.1
flake8-docstrings
black >= 23.1.0
pytest
mypy
pylint >= 2.16.0
pysnmplib
pysnmp-pysmi
