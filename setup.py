from setuptools import setup, find_packages

# see details in https://github.com/pypa/sampleproject/blob/master/setup.py

setup(
    use_scm_version={"write_to": "version.txt"},
)
